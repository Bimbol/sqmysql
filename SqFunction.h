#pragma once

SQFUNC(Sq_mysql_connect);
SQFUNC(Sq_mysql_close);
SQFUNC(Sq_mysql_query);
SQFUNC(Sq_mysql_num_rows);
SQFUNC(Sq_mysql_num_fields);
SQFUNC(Sq_mysql_fetch_row);
SQFUNC(Sq_mysql_fetch_assoc);
SQFUNC(Sq_mysql_ping);
SQFUNC(Sq_mysql_free_result);
SQFUNC(Sq_mysql_error);
SQFUNC(Sq_mysql_errno);
