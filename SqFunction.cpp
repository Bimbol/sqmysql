#include "PCH.h"

SQFUNC(Sq_mysql_connect)
{
	int iArgs = g_pApi->gettop(vm) - 1;
	if (iArgs < 4 || iArgs > 5)
		return g_pApi->throwerror(vm, "(mysql_connect) wrong number of parameters, expecting 4");

	// Check types
	if (g_pApi->gettype(vm, 2) != OT_STRING)
		return g_pApi->throwerror(vm, "(mysql_connect) wrong type of parameter 0, expecting 'string'");

	if (g_pApi->gettype(vm, 3) != OT_STRING)
		return g_pApi->throwerror(vm, "(mysql_connect) wrong type of parameter 1, expecting 'string'");

	if (g_pApi->gettype(vm, 4) != OT_STRING)
		return g_pApi->throwerror(vm, "(mysql_connect) wrong type of parameter 2, expecting 'string'");

	if (g_pApi->gettype(vm, 5) != OT_STRING)
		return g_pApi->throwerror(vm, "(mysql_connect) wrong type of parameter 3, expecting 'string'");

	const SQChar *sHost;
	const SQChar *sUser;
	const SQChar *sPass;
	const SQChar *sDatabase;
	SQInteger iPort = 3306;

	g_pApi->getstring(vm, 2, &sHost);
	g_pApi->getstring(vm, 3, &sUser);
	g_pApi->getstring(vm, 4, &sPass);
	g_pApi->getstring(vm, 5, &sDatabase);

	if (g_pApi->gettype(vm, 6) == OT_INTEGER)
		g_pApi->getinteger(vm, 6, &iPort);

	MYSQL *pHandler = mysql_init(NULL);

	if (!pHandler)
	{
		g_pApi->pushnull(vm);
		return 1;
	}

	if (!mysql_real_connect(pHandler, sHost, sUser, sPass, sDatabase, iPort, NULL, 0))
	{
		mysql_close(pHandler);
		g_pApi->pushnull(vm);

		return 1;
	}

	g_pApi->pushuserpointer(vm, pHandler);
	return 1;
}

SQFUNC(Sq_mysql_close)
{
	if (g_pApi->gettop(vm) != 2)
		return g_pApi->throwerror(vm, "(mysql_close) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, -1) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_close) wrong type of parameter 0, expecting 'userpointer'");

	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, -1, &pPointer);

	if (pPointer)
	{
		MYSQL *pHandler = reinterpret_cast<MYSQL*>(pPointer);
		mysql_close(pHandler);
	}

	return 0;
}

SQFUNC(Sq_mysql_query)
{
	if (g_pApi->gettop(vm) != 3)
		return g_pApi->throwerror(vm, "(mysql_query) wrong number of parameters, expecting 2");

	if (g_pApi->gettype(vm, -2) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_query) wrong type of parameter 0, expecting 'userpointer'");

	if (g_pApi->gettype(vm, -1) != OT_STRING)
		return g_pApi->throwerror(vm, "(mysql_query) wrong type of parameter 1, expecting 'string'");

	SQUserPointer pPointer;
	const SQChar *sQuery;

	g_pApi->getuserpointer(vm, -2, &pPointer);
	g_pApi->getstring(vm, -1, &sQuery);
	
	if (!pPointer)
	{
		g_pApi->pushnull(vm);
		return 1;
	}

	MYSQL *pHandler = reinterpret_cast<MYSQL*>(pPointer);
	MYSQL_RES *pResult = NULL;

	if (mysql_query(pHandler, sQuery))
	{
		g_pApi->pushnull(vm);
		return 1;
	}

	pResult = mysql_store_result(pHandler);
	if (!pResult)
	{
		g_pApi->pushnull(vm);
		return 1;
	}

	g_pApi->pushuserpointer(vm, pResult);
	return 1;
}

SQFUNC(Sq_mysql_num_rows)
{
	if (g_pApi->gettop(vm) != 2)
		return g_pApi->throwerror(vm, "(mysql_num_rows) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, -1) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_num_rows) wrong type of parameter 0, expecting 'userpointer'");

	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, -1, &pPointer);

	int numRows = 0;
	if (pPointer)
	{
		MYSQL_RES *pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
		numRows = mysql_num_rows(pResult);
	}

	g_pApi->pushinteger(vm, numRows);
	return 1;
}

SQFUNC(Sq_mysql_num_fields)
{
	if (g_pApi->gettop(vm) != 2)
		return g_pApi->throwerror(vm, "(mysql_num_fields) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, -1) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_num_fields) wrong type of parameter 0, expecting 'userpointer'");


	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, -1, &pPointer);

	int numFields = 0;
	if (pPointer)
	{
		MYSQL_RES *pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
		numFields = mysql_num_fields(pResult);
	}

	g_pApi->pushinteger(vm, numFields);
	return 1;
}

SQFUNC(Sq_mysql_fetch_row)
{
	if (g_pApi->gettop(vm) != 2)
		return g_pApi->throwerror(vm, "(mysql_fetch_row) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, -1) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_fetch_row) wrong type of parameter 0, expecting 'userpointer'");

	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		g_pApi->pushnull(vm);
		return 1;
	}

	MYSQL_RES *pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
	MYSQL_FIELD *pFields = mysql_fetch_fields(pResult);
	MYSQL_ROW pRow = mysql_fetch_row(pResult);

	if (!pFields || !pRow)
	{
		g_pApi->pushnull(vm);
		return 1;
	}
	
	g_pApi->newarray(vm, 0);

	int numFields = mysql_num_fields(pResult);
	for (int i = 0; i < numFields; ++i)
	{
		if (!pRow[i]) pFields[i].type = MYSQL_TYPE_NULL;

		switch (pFields[i].type)
		{
			case MYSQL_TYPE_TINY:
			case MYSQL_TYPE_SHORT:
			case MYSQL_TYPE_LONG:
			case MYSQL_TYPE_LONGLONG:
			case MYSQL_TYPE_INT24:
			case MYSQL_TYPE_YEAR:
			case MYSQL_TYPE_BIT:
				g_pApi->pushinteger(vm, atoi(pRow[i]));
				g_pApi->arrayappend(vm, -2);
				break;

			case MYSQL_TYPE_NULL:
				g_pApi->pushnull(vm);
				g_pApi->arrayappend(vm, -2);
				break;

			case MYSQL_TYPE_DECIMAL:
			case MYSQL_TYPE_NEWDECIMAL:
			case MYSQL_TYPE_FLOAT:
			case MYSQL_TYPE_DOUBLE:
				g_pApi->pushfloat(vm, atof(pRow[i]));
				g_pApi->arrayappend(vm, -2);
				break;

			default:
				g_pApi->pushstring(vm, pRow[i], -1);
				g_pApi->arrayappend(vm, -2);
				break;
		}
	}

	return 1;
}

SQFUNC(Sq_mysql_fetch_assoc)
{
	if (g_pApi->gettop(vm) != 2)
		return g_pApi->throwerror(vm, "(mysql_fetch_assoc) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, -1) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_fetch_assoc) wrong type of parameter 0, expecting 'userpointer'");

	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, -1, &pPointer);

	if (!pPointer)
	{
		g_pApi->pushnull(vm);
		return 1;
	}

	MYSQL_RES *pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
	MYSQL_FIELD *pFields = mysql_fetch_fields(pResult);
	MYSQL_ROW pRow = mysql_fetch_row(pResult);

	if (!pFields || !pRow)
	{
		g_pApi->pushnull(vm);
		return 1;
	}
				
	g_pApi->newtable(vm);

	int numFields = mysql_num_fields(pResult);
	for (int i = 0; i < numFields; ++i)
	{
		if (!pRow[i]) pFields[i].type = MYSQL_TYPE_NULL;

		switch (pFields[i].type)
		{
			case MYSQL_TYPE_TINY:
			case MYSQL_TYPE_SHORT:
			case MYSQL_TYPE_LONG:
			case MYSQL_TYPE_LONGLONG:
			case MYSQL_TYPE_INT24:
			case MYSQL_TYPE_YEAR:
			case MYSQL_TYPE_BIT:
				g_pApi->pushstring(vm, pFields[i].name, -1);
				g_pApi->pushinteger(vm, atoi(pRow[i]));
				g_pApi->newslot(vm, -3, SQFalse);
				break;

			case MYSQL_TYPE_NULL:
				g_pApi->pushstring(vm, pFields[i].name, -1);
				g_pApi->pushnull(vm);
				g_pApi->newslot(vm, -3, SQFalse);
				break;

			case MYSQL_TYPE_DECIMAL:
			case MYSQL_TYPE_NEWDECIMAL:
			case MYSQL_TYPE_FLOAT:
			case MYSQL_TYPE_DOUBLE:
				g_pApi->pushstring(vm, pFields[i].name, -1);
				g_pApi->pushfloat(vm, atof(pRow[i]));
				g_pApi->newslot(vm, -3, SQFalse);
				break;

			default:
				g_pApi->pushstring(vm, pFields[i].name, -1);
				g_pApi->pushstring(vm, pRow[i], -1);
				g_pApi->newslot(vm, -3, SQFalse);
				break;
		}
	}

	return 1;
}

SQFUNC(Sq_mysql_ping)
{
	if (g_pApi->gettop(vm) != 2)
		return g_pApi->throwerror(vm, "(mysql_ping) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, -1) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_ping) wrong type of parameter 0, expecting 'userpointer'");

	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, -1, &pPointer);
	
	bool result = false;
	if (pPointer)
	{
		MYSQL *pHandler = reinterpret_cast<MYSQL*>(pPointer);
		result = !mysql_ping(pHandler);
	}

	g_pApi->pushbool(vm, result);
	return 1;
}

SQFUNC(Sq_mysql_free_result)
{
	if (g_pApi->gettop(vm) != 2)
		return g_pApi->throwerror(vm, "(mysql_free_result) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, -1) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_free_result) wrong type of parameter 0, expecting 'userpointer'");

	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, -1, &pPointer);

	if (pPointer)
	{
		MYSQL_RES *pResult = reinterpret_cast<MYSQL_RES*>(pPointer);
		mysql_free_result(pResult);
	}

	return 0;
}

SQFUNC(Sq_mysql_error)
{
	if (g_pApi->gettop(vm) != 2)
		return g_pApi->throwerror(vm, "(mysql_error) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, -1) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_error) wrong type of parameter 0, expecting 'userpointer'");

	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, -1, &pPointer);

	if (pPointer)
	{
		MYSQL *pHandler = reinterpret_cast<MYSQL*>(pPointer);
		const char *sErrMsg = mysql_error(pHandler);

		g_pApi->pushstring(vm, sErrMsg, -1);
		return 1;
	}

	g_pApi->pushnull(vm);
	return 1;
}

SQFUNC(Sq_mysql_errno)
{
	if (g_pApi->gettop(vm) != 2)
		return g_pApi->throwerror(vm, "(mysql_errno) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, -1) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(mysql_errno) wrong type of parameter 0, expecting 'userpointer'");

	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, -1, &pPointer);

	int errId = 0;

	if (pPointer)
	{
		MYSQL *pHandler = reinterpret_cast<MYSQL*>(pPointer);
		errId = mysql_errno(pHandler);
	}

	g_pApi->pushinteger(vm, errId);
	return 1;
}