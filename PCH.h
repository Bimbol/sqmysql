#pragma once

#ifdef WIN32
#include <windows.h>
#endif

#include <mysql.h>
#include <stdlib.h>
#include <iostream>

#include "Squirrel/sqmodule.h"
#include "Squirrel/SqApi.h"

extern HSQAPI g_pApi;

#include "SqFunction.h"