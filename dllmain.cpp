#include "PCH.h"

HSQAPI g_pApi = NULL;

void SqRegisterFunction(HSQUIRRELVM vm, const char *name, SQFUNCTION func)
{
	g_pApi->pushroottable(vm);
	g_pApi->pushstring(vm, name, -1);
	g_pApi->newclosure(vm, func, 0);
	g_pApi->newslot(vm, -3, SQFalse);
	g_pApi->pop(vm, 1);
}

#ifdef __cplusplus
extern "C" {
#endif
SQRESULT EXPORT sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	std::cout << "MySQL module for G2O v0.2" << std::endl;

	g_pApi = api;

	SqRegisterFunction(vm, "mysql_connect", Sq_mysql_connect);
	SqRegisterFunction(vm, "mysql_close", Sq_mysql_close);
	SqRegisterFunction(vm, "mysql_query", Sq_mysql_query);
	SqRegisterFunction(vm, "mysql_num_rows", Sq_mysql_num_rows);
	SqRegisterFunction(vm, "mysql_num_fields", Sq_mysql_num_fields);
	SqRegisterFunction(vm, "mysql_fetch_row", Sq_mysql_fetch_row);
	SqRegisterFunction(vm, "mysql_fetch_assoc", Sq_mysql_fetch_assoc);
	SqRegisterFunction(vm, "mysql_ping", Sq_mysql_ping);
	SqRegisterFunction(vm, "mysql_free_result", Sq_mysql_free_result);
	SqRegisterFunction(vm, "mysql_error", Sq_mysql_error);
	SqRegisterFunction(vm, "mysql_errno", Sq_mysql_errno);

	return SQ_OK;
}
#ifdef __cplusplus
} /*extern "C"*/
#endif